My PayPal 💰
    | https://paypal.me/KaukinVK
    | KaukinVK@ya.ru

Thanks to all who donated 🎉
    It is really nice.

Targeted fundraising 🎯
    | 3k$ for create documentation. Style: https://alabaster.readthedocs.io/en/latest/
    | Considering the dynamics - in ~100 years :D
    | So you'd better buy strings for my balalaika and meat for my bear.

Do not forget to star imap_tools project ⭐
    https://github.com/ikvk/imap_tools
